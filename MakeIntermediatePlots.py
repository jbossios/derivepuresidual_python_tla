from ROOT import *
import os,sys

# PATH to ROOT files
PATH = "REPLACE/derivepuresidual_python_tla/Outputs"

Terms = [
  "Mu",
#  "NPV",
#  "nJet20",
#  "nJet10",
]

# PATH to AtlasStyle.C
PATHtoAtlasStyle = "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle"

# Version used in config file
Version = "TLA_17102019_PythiaWithSW_AfterAreaCorr_MC16All_NewPtTruthBin_useConstit_NPV" # Example

JetCollection = "AntiKt4EMTopoJets"

Debug            = False
TLA              = True
FineEtaBinning   = True  # Offline/TLA (high-stat) eta binning
CoarseEtaBinning = False # HLT FTK eta binning
PaperEtaBinning  = False
nMuBins     = 11
#nNJet20Bins = 6 # LATEST
#nNPVBins    = 5 # LATEST
nNJet20Bins = 1
nNPVBins    = 1

#################################################
## DO NOT MODIFY
#################################################

PATH += "/"
PATHtoAtlasStyle += "/"
gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro(PATHtoAtlasStyle+"AtlasStyle.C")
gROOT.ProcessLine(".L loader.C+") # So I can read vector<vector<float> > branches
gStyle.SetOptStat(0)
SetAtlasStyle()

if FineEtaBinning and CoarseEtaBinning:
  print "Choose one, FineEtaBinning, CoarseEtaBinning or PaperEtaBinning"
  sys.exit(0)
if FineEtaBinning and PaperEtaBinning:
  print "Choose one, FineEtaBinning, CoarseEtaBinning or PaperEtaBinning"
  sys.exit(0)
if CoarseEtaBinning and PaperEtaBinning:
  print "Choose one, FineEtaBinning, CoarseEtaBinning or PaperEtaBinning"
  sys.exit(0)

nEtaBins = 41
if FineEtaBinning:
  EtaBins = ["0.0","0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9","1.0","1.1","1.2","1.3","1.4","1.5","1.6","1.7","1.8","1.9","2.0","2.1","2.2","2.3","2.4","2.5","2.6","2.7","2.8","2.9","3.0","3.1","3.2","3.3","3.4","3.5","3.6","3.8","4.0","4.2","4.5","4.9"]
if CoarseEtaBinning:
  nEtaBins = 21	
  EtaBins = ["0.0","0.2","0.4","0.6","0.8","1.0","1.2","1.4","1.6","1.8","2.0","2.2","2.4","2.6","2.8","3.0","3.2","3.4","3.6","4.0","4.5","4.9"]
if PaperEtaBinning:
  nEtaBins = 10
  EtaBins = ["0.0","0.3","0.8","1.2","1.6","2.1","2.4","2.8","3.2","3.6","4.5"]

#################################
# pTresidual plots
# Loop over terms
for term in Terms:
  # TCanvas	
  can = TCanvas()
  # Name of output PDF
  outPDF = PATH+Version+"_pTresidualVS"+term+".pdf"
  can.Print(outPDF+"[")
  # Open file
  FileName = PATH + "TH1D_pTresidual_vs_term_" + Version + ".root"
  File     = TFile.Open(FileName,"READ")
  if not File:
    print FileName + " not found, exiting"
    sys.exit(0)
  # Loop over eta bins
  for ieta in range(0,nEtaBins):
    # Loop over mu/npv bins
    nBins = nNPVBins
    if TLA: nBins = nNJet20Bins
    if term=="NPV" or term=="nJet20" or term=="nJet10":
      nBins = nMuBins
    for ibin in range(nBins):
      # Get TH1D
      HistName  = "MeanPtResidual_vs_"
      HistName += term
      HistName += "_ptbin_0_etabin_"
      HistName += str(ieta)
      if term == "Mu":
        if TLA:
          HistName += "_nJetbin_"
	else:
          HistName += "_NPVbin_"
      else: 
        HistName += "_Mubin_"
      HistName += str(ibin)
      #HistName += "_mean"
      Hist = File.Get(HistName)
      if not Hist:
        print HistName + " not found, exiting"	      
	#sys.exit(0)
	continue
      Hist.SetTitle("")
      if term == "Mu":
        Hist.GetXaxis().SetTitle("#mu")
      else:
	if TLA:
          Hist.GetXaxis().SetTitle("n_{20}^{jet}")
        else:
          Hist.GetXaxis().SetTitle("N_{PV}")
      Hist.GetYaxis().SetTitle("#it{p}_{T}^{residual} [GeV]")
      if not Hist:
        print HistName + " not found, exiting"
	sys.exit(0)
      # Get Fit
      #if Debug:
      #  print "GetFunction("+HistName+"_myfit)"
      #Hist.GetFunction(HistName+"_myfit").SetLineColor(kRed)
      #Hist.GetFunction(HistName+"_myfit").SetMarkerColor(kRed)
      # Draw
      Hist.Draw()
      # Show eta bin
      if ieta==0:
         etaBin = "|#eta|<"+str(EtaBins[ieta+1])
      else:
         etaBin = str(EtaBins[ieta])+"<|#eta|<"+str(EtaBins[ieta+1])
      TextBlock_Eta = TLatex(0.2,0.88,etaBin)
      TextBlock_Eta.SetNDC()
      TextBlock_Eta.Draw("same")
      # Show NPV/mu bin
      if TLA:
        Bin = "n_{20}^{jet} bin " + str(ibin+1)
      else:
        Bin = "N_{PV} bin " + str(ibin+1)
      if term == "NPV" or term=="nJet20" or term=="nJet10":
        Bin = "#mu bin " + str(ibin+1)
      TextBlock_Bin = TLatex(0.2,0.83,Bin)
      TextBlock_Bin.SetNDC()
      TextBlock_Bin.Draw("same")
      can.Print(outPDF)
  can.Print(outPDF+"]")


#################################
# dpTresidual/dTerm plots
# Loop over terms
for term in Terms:
  # TCanvas
  can = TCanvas()
  # Name of output PDF
  outPDF = PATH+Version+"_dpTresidualOVERd"+term+".pdf"
  can.Print(outPDF+"[")
  # Open file
  #FileName = PATH + term + "Term" + Version + JetCollection + ".root"
  FileName = PATH + "TH1D_pTresidual_vs_term_" + Version + ".root"
  File     = TFile.Open(FileName,"READ")
  if not File:
    print FileName + " not found, exiting"
    sys.exit(0)
  # Loop over eta bins
  for ieta in range(0,nEtaBins):
    HistName  = "dpTresidual_d"
    HistName += term
    HistName += "_vs_otherterm"
    HistName += "_ptbin_0_etabin_"
    HistName += str(ieta)
    Hist = File.Get(HistName)
    if not Hist:
      print HistName + " not found, exiting"
      sys.exit(0)
    Hist.SetTitle("")
    if term == "Mu":
      if TLA:
        Hist.GetXaxis().SetTitle("n_{20}^{jet}")
      else:
        Hist.GetXaxis().SetTitle("N_{PV}")
      Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial#mu [GeV]")
    else:
      Hist.GetXaxis().SetTitle("#mu")
      if TLA:
        #Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial n_{20}^{jet} [GeV]")
        Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial n_{10}^{jet} [GeV]")
      else:
        Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial N_{PV} [GeV]")
    # Get Fit
    #Hist.GetFunction(HistName+"_myfit").SetLineColor(kRed)
    #Hist.GetFunction(HistName+"_myfit").SetMarkerColor(kRed)
    Hist.Draw()
    # Show eta bin
    if ieta==0:
       etaBin = "|#eta|<"+str(EtaBins[ieta+1])
    else:
       etaBin = str(EtaBins[ieta])+"<|#eta|<"+str(EtaBins[ieta+1])
    TextBlock_Eta = TLatex(0.2,0.88,etaBin)
    TextBlock_Eta.SetNDC()
    TextBlock_Eta.Draw("same")
    can.Print(outPDF)
  can.Print(outPDF+"]")

print ">>> ALL DONE <<<"
