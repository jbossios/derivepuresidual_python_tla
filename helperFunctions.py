import os,sys,config
RecoPt = float(0)
def SetBranches(Tree,Cfg):
  Tree.SetBranchAddress(Cfg.CaloPt,RecoPt)
def GetConfig(Config):
  message = "INFO: Configuration file: "+Config+"\n"
  configFound = False
  Error       = False
  cfg         = config.Config()
  if os.path.isfile("Configs/"+Config):
    with open("Configs/"+Config) as f:
      code = compile(f.read(), "Configs/"+Config, 'exec')
      global_vars, local_vars = {}, {}
      exec(code, global_vars, local_vars)
      for k,v in local_vars.iteritems():
        if isinstance(v,config.Config):
          cfg = v
  	  configFound = True
          break
  else: # config file not found
    message += "ERROR: Configs/"+Config+" does not exists, exiting\n"
    Error    = True
  if not configFound: # no Config class was found
    message += "ERROR: no instance of config.Config() was found in Configs/"+Config+", exiting\n"
    Error    = True
  return message,Error,cfg
