import os,sys,config,ROOT,array
from helperFunctions import *
ROOT.gROOT.SetBatch(True)  # so does not pop up plots!

# Terms
Terms = [
  "Mu",
  #"nJet10",
  "NPV",
]

# Input ROOT File(s)
InputFiles = [
#  "PATH/File1.root", # Example 1
#  "PATH/File2.root", # Example 2
########################################
# TTrees used to derive the calibration
########################################
# MC16a PythiaWithSW
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364701.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ1_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364702.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ2_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364703.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ3_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364704.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ4_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364705.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ5_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364706.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ6_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364707.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ7_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364708.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ8_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364709.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ9_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364710.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ10_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364711.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ11_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16a_PythiaWithSW/user.jbossios.364712.PythiaWithSWPUresidual_MC16a_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ12_tree.root",
# MC16d PythiaWithSW
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364701.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ1_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364702.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ2_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364703.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ3_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364704.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ4_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364705.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ5_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364706.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ6_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364707.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ7_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364708.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ8_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364709.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ9_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364710.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ10_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364711.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ11_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16d_PythiaWithSW/user.jbossios.364712.PythiaWithSWPUresidual_MC16d_PythiaWithSW_TLA_FinalNjetDef_050919_tree.root/JZ12_tree.root",
# MC16e PythiaWithSW
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364701.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ1_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364702.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ2_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364703.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ3_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364704.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ4_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364705.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ5_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364706.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ6_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364707.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ7_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364708.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ8_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364709.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ9_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364710.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ10_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364711.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ11_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/NoResidual/MC16e_PythiaWithSW/user.jbossios.364712.PythiaWithSWPUresidual_MC16e_PythiaWithSW_TLA_SeveralNjetOptions_BiasStudy_220819_tree.root/JZ12_tree.root",
#################################
# TTrees used to see the closure
#################################
# MC16a PythiaWithSW
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364701.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ1_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364702.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ2_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364703.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ3_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364704.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ4_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364705.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ5_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364706.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ6_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364707.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ7_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364708.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ8_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364709.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_ReSend_261119_tree.root/JZ9_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364710.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ10_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364711.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ11_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16a_PythiaWithSW/user.jbossios.364712.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16a_Closure_251119_tree.root/JZ12_tree.root",
# MC16d PythiaWithSW
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364701.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ1_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364702.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ2_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364703.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ3_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364704.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ4_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364705.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ5_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364706.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ6_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364707.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ7_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364708.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ8_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364709.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ9_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364710.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ10_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364711.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ11_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16d_PythiaWithSW/user.jbossios.364712.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16d_Closure_251119_tree.root/JZ12_tree.root",
# MC16e PythiaWithSW
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364701.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ1_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364702.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ2_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364703.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ3_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364704.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ4_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364705.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ5_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364706.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ6_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364707.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ7_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364708.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ8_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364709.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ9_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364710.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ10_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364711.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ11_tree.root",
#  "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/TLA/Calibrations/TTrees/AfterPUresidual/MC16e_PythiaWithSW/user.jbossios.364712.PythiaWithSWMCJES_AfterMuResidual_avgNPV_MC16e_Closure_251119_tree.root/JZ12_tree.root",
]

# Configuration File Name
#Config = 'TLA_Example.py' # Example
# BeforeAreaCorr (only for mu and NPV)
#Config = 'TLA_02122019_PythiaWithSW_BeforeAreaCorr_MC16a_NewPtTruthBin_NPV.py'
#Config = 'TLA_02122019_PythiaWithSW_BeforeAreaCorr_MC16d_NewPtTruthBin_NPV.py'
#Config = 'TLA_02122019_PythiaWithSW_BeforeAreaCorr_MC16e_NewPtTruthBin_NPV.py'
#Config = 'TLA_02122019_PythiaWithSW_BeforeAreaCorr_MC16All_NewPtTruthBin_NPV.py'
# NoPUresidual (only for mu and NPV)
#Config = 'TLA_17102019_PythiaWithSW_AfterAreaCorr_MC16a_NewPtTruthBin_useConstit_NPV.py'
#Config = 'TLA_17102019_PythiaWithSW_AfterAreaCorr_MC16d_NewPtTruthBin_useConstit_NPV.py'
#Config = 'TLA_17102019_PythiaWithSW_AfterAreaCorr_MC16e_NewPtTruthBin_useConstit_NPV.py'
#Config = 'TLA_17102019_PythiaWithSW_AfterAreaCorr_MC16All_NewPtTruthBin_useConstit_NPV.py'
# After full pileup residual (just mu-based corr using mu and NPV)
#Config = 'TLA_02122019_PythiaWithSW_AfterMuPUresidual_MC16a_NewPtTruthBin_NPV.py'
#Config = 'TLA_02122019_PythiaWithSW_AfterMuPUresidual_MC16d_NewPtTruthBin_NPV.py'
#Config = 'TLA_02122019_PythiaWithSW_AfterMuPUresidual_MC16e_NewPtTruthBin_NPV.py'
#Config = 'TLA_02122019_PythiaWithSW_AfterMuPUresidual_MC16All_NewPtTruthBin_NPV.py'

FillpTresidualHists  = False # This runs over the TTree(s) and fills the pTresidual histograms
MergepTresidualHists = False # You need to run this after running FillpTresidualHists
MergeMC16campaigns   = False
DeriveCalibration    = True # This does all the fitting and derive the calibration (needs the previous step)

doFinalVSetaFit = False

Debug = False

###################################################################################
## DO NOT MODIFY
###################################################################################

##########################################################
# Protections
Mu     = False
NPV    = False
nJet   = False
for term in Terms:
  if term == "Mu": Mu = True
  elif term == "NPV": NPV = True
  elif term == "nJet10": nJet = True
  else:
    print "ERROR: "+term+" not recognised, exiting"
    sys.exit(0)
if NPV and nJet:
  print "ERROR: It is not possible to run NPV and nJet10 terms simultaneously, pick one, exiting"
  sys.exit(0)

if FillpTresidualHists and MergepTresidualHists:
  print "ERROR: FillpTresidualHists and MergepTresidualHists can not be true at the same time, exiting"
  sys.exit(0)
if FillpTresidualHists and DeriveCalibration:
  print "ERROR: FillpTresidualHists and DeriveCalibration can not be true at the same time, exiting"
  sys.exit(0)
if DeriveCalibration and MergepTresidualHists:
  print "ERROR: DeriveCalibration and MergepTresidualHists can not be true at the same time, exiting"
  sys.exit(0)


##########################################################
# Useful things
MeVtoGeV          = 0.001
nPtResidualBins   = 200
MinPtResidual     = -400.
MaxPtResidual     = 400.
PtResidualBins    = []
MinVar            = 0   # min value for Mu, NPV and nJet10 for TH3D histogram (user's binning is used for fitting)
MaxVar            = 200 # max value for Mu, NPV and nJet10 for TH3D histogram (user's binning is used for fitting)
nVarBins          = 200 # number of Mu, NPV and nJet10 bins for TH3D histogram (user's binning is used for fitting)
VarBins           = []
for ibin in range(nVarBins+1):
  VarBins.append(MinVar+ibin*(MaxVar-MinVar)/nVarBins)
VarBinning = array.array('d',VarBins)
for ibin in range(nPtResidualBins+1):
  PtResidualBins.append(MinPtResidual+ibin*(MaxPtResidual-MinPtResidual)/nPtResidualBins)
PtResidualBinning = array.array('d',PtResidualBins)


##########################################################
# Fill pTresidual histograms by running over the TTree(s)
##########################################################
if FillpTresidualHists:

  # Fill TH3D distributions of pTresidual vs mu and NPV/nJet10 for each pTtruth and absEtaTruth bins

  ##########################################################
  # Loop over TTrees
  for inputFile in InputFiles:

    ##########################################################
    # Get configuration file
    message,Err,cfg = GetConfig(Config)
    if Err:
      print message
      sys.exit(0)

    # Protection
    if cfg.ApplyAreaCorr:
      print('ERROR: application of area-based correction is not supported anymore (need to be fixed), exiting')
      sys.exit(0)

    ##########################################################
    # Extract name of the input file which will be used as name of the output file
    outputName_tmp = inputFile.split("/")
    outputName     = outputName_tmp[len(outputName_tmp)-1]
    LogFileName    = "Outputs/TH3D_pTresidual_"+cfg.Version+"_"+outputName
    LogFileName    = LogFileName.replace(".root",".log")
    LogFile        = open(LogFileName,'w')

    ##########################################################
    # Print configuration
    LogFile.write(message)
    LogFile.write(cfg.Print())

    ##########################################################
    # Open TFile and get TTree
    LogFile.write("INFO: Opening "+inputFile+"\n")
    iFile = ROOT.TFile.Open(inputFile,'READ')
    if not iFile:
      LogFile.write("ERROR: "+inputFile+" not found, exiting\n")
      sys.exit(0)
    tree  = iFile.Get(cfg.TreeName)
    if not tree:
      LogFile.write("ERROR: "+cfg.TreeName+" not found, exiting\n")
      sys.exit(0)
    LogFile.write("INFO: Number of entries: "+str(tree.GetEntries())+"\n")

    ##########################################################
    # Read pT, eta and mu/npv/njet20 binning
    nPtBins       = len(cfg.PtBins)-1
    nAbsEtaBins   = len(cfg.InitialAbsEtaBins)-1
    nVar1Bins     = len(cfg.Variable1Bins)-1   # NPV or nJet10
    nVar2Bins     = len(cfg.Variable2Bins)-1   # Mu
    Var1Binning   = array.array('d',cfg.Variable1Bins) # NPV or nJet10
    Var2Binning   = array.array('d',cfg.Variable2Bins) # Mu
    
    ##########################################################
    # Book histograms
    h_pTresidual  = {} # Dictionary with pTresidual histograms
    for ptbin in range(nPtBins): # loop over pTruth bins
      for etabin in range(nAbsEtaBins): # loop over abs eta truth bins
        HistName  = "pTresidual_vs_"
	if NPV:
          HistName += "NPV"
	else:
          HistName += "nJet"
        HistName += "_vs_Mu_ptbin_"+str(ptbin)+"_etabin_"+str(etabin)
        Hist      = ROOT.TH3D(HistName,"",nVarBins,VarBinning,nVarBins,VarBinning,nPtResidualBins,PtResidualBinning)
        h_pTresidual['ptbin_'+str(ptbin)+"_etabin_"+str(etabin)] = Hist
    
    ##########################################################
    # Loop over entries in the TTree
    eventCounter = 0
    for event in tree:
 
      eventCounter += 1
      if eventCounter % 100000 == 0: LogFile.write("INFO: "+str(eventCounter)+" events processed\n")
    
      ##############################################
      # Get variables
      RecoPt  = getattr(event, cfg.RecoPt)  # pT of all reco jets that match a truth jet
      TruePt  = getattr(event, cfg.TruePt)  # pT of the truth jet that matches each reco jet
      TrueEta = getattr(event, cfg.TrueEta) # eta of the truth jet that matches each reco jet
      if cfg.ApplyAreaCorr: Rho = getattr(event, cfg.Rho)
      nJets   = len(RecoPt)
      Var1 = getattr(event,cfg.Variable1)   # NPV or nJet10
      Var2 = getattr(event,cfg.Variable2)   # mu

      ##############################################
      # Loop over reco jets
      for ijet in range(nJets):

        ##############################################
        # Loop over eta bins
        for etabin in range(nAbsEtaBins):

          AbsEtaMin = cfg.InitialAbsEtaBins[etabin]
          AbsEtaMax = cfg.InitialAbsEtaBins[etabin+1]

          ################################################
          # Check if absEta of matched truth jet is within eta bin
          absEta = TrueEta[ijet]
          if absEta>= AbsEtaMin and absEta<AbsEtaMax:

            ##############################################
            # Loop over pt bins
            for ptbin in range(nPtBins):
    
              TruthMinPt = cfg.PtBins[ptbin]
              TruthMaxPt = cfg.PtBins[ptbin+1]
    
              ##################################################
              # Check if matched truth jet is within the pT bin
              truthPt = TruePt[ijet]
              if truthPt>= TruthMinPt and truthPt<TruthMaxPt:

                ##############################################
                # Compute pTresidual
                recoPt = RecoPt[ijet]
                if cfg.ApplyAreaCorr: rho = Rho[ijet]*MeVtoGeV
                if Debug: LogFile.write("DEBUG: RecoPt: "+str(recoPt)+"\n")
                if Debug: LogFile.write("DEBUG: RecoAbsEta: "+str(absEta)+"\n")
                if Debug and cfg.ApplyAreaCorr: LogFile.write("DEBUG: Rho: "+str(rho)+"\n")
                pTresidual = recoPt - rho - truthPt if cfg.ApplyAreaCorr else recoPt - truthPt
                if Debug: LogFile.write("DEBUG: pTresidual: "+str(pTresidual)+"\n")
    
                ##########################################################
                # Fill the corresponding pTresidual histogram
                h_pTresidual['ptbin_'+str(ptbin)+"_etabin_"+str(etabin)].Fill(Var1,Var2,pTresidual)

    LogFile.write("INFO: All done")
    LogFile.close()

    ##########################################################
    # Write histograms to a ROOT file
    oFile = ROOT.TFile("Outputs/TH3D_pTresidual_"+cfg.Version+"_"+outputName,"RECREATE")
    for hist in h_pTresidual.values(): hist.Write()
    oFile.Close()


###############################################################################
# Merge ROOT files from previous step
###############################################################################
if MergepTresidualHists:
  message,Err,cfg = GetConfig(Config)
  print message
  if Err: sys.exit(0)
  print cfg.Print()
  # Merge all the ROOT files
  command = "hadd Outputs/TH3D_pTresidual_"+cfg.Version+".root "
  for inputFile in InputFiles: # loop over input files
    # Extract name of the input file
    outputName_tmp = inputFile.split("/")
    outputName     = outputName_tmp[len(outputName_tmp)-1]
    command       += "Outputs/TH3D_pTresidual_"+cfg.Version+"_"+outputName+" "
  os.system(command)
  print "INFO: All Done"


###############################################################################
# Merge ROOT files from all MC16a campaigns
###############################################################################
if MergeMC16campaigns:
  print "Merging all MC16 campaign outputs"
  if "MC16a" in Config:
    MC16a = True
    MC16d = False
    MC16e = False
    MC16aConfig = Config
  elif "MC16d" in Config:
    MC16d = True
    MC16a = False
    MC16e = False
    MC16dConfig = Config
  elif "MC16e" in Config:
    MC16e = True
    MC16a = False
    MC16d = False
    MC16eConfig = Config
  if MC16a:
    MC16dConfig   = Config.replace("MC16a","MC16d")
    MC16eConfig   = Config.replace("MC16a","MC16e")
    MC16AllConfig = Config.replace("MC16a","MC16All")
  elif MC16d:
    MC16aConfig   = Config.replace("MC16d","MC16a")
    MC16eConfig   = Config.replace("MC16d","MC16e")
    MC16AllConfig = Config.replace("MC16d","MC16All")
  elif MC16e:
    MC16aConfig   = Config.replace("MC16e","MC16a")
    MC16dConfig   = Config.replace("MC16e","MC16d")
    MC16AllConfig = Config.replace("MC16e","MC16All")
  # Get all the versions
  messageA,ErrA,cfgA = GetConfig(MC16aConfig)
  print messageA
  if ErrA: sys.exit(0)
  messageD,ErrD,cfgD = GetConfig(MC16dConfig)
  print messageD
  if ErrD: sys.exit(0)
  messageE,ErrE,cfgE = GetConfig(MC16eConfig)
  print messageE
  if ErrE: sys.exit(0)
  message,Err,cfg = GetConfig(MC16AllConfig)
  print message
  if Err: sys.exit(0)
  command = "hadd Outputs/TH3D_pTresidual_"+cfg.Version+".root Outputs/TH3D_pTresidual_"+cfgA.Version+".root Outputs/TH3D_pTresidual_"+cfgD.Version+".root Outputs/TH3D_pTresidual_"+cfgE.Version+".root"
  os.system(command)
  
 
###############################################################################
# Read pTresidual histograms and perform the fits to derive the calibration(s)
###############################################################################
if DeriveCalibration:
  
  ##########################################################
  # Get config file
  message,Err,cfg = GetConfig(Config)
  print message # print INFO and ERROR messages from GetConfig()
  if Err: sys.exit(0) # exit if error ocurred when openning config file
  print cfg.Print() # print configuration

  ##########################################################
  # Get TFile with the histograms
  iFileName = "Outputs/TH3D_pTresidual_"+cfg.Version+".root"
  iFile     = ROOT.TFile(iFileName,"READ")
  if not iFile:
    print "ERROR: "+iFileName+" not found, exiting"
    sys.exit(0)

  ##########################################################
  # Set arrays for intermediate and final histograms
  pTresidual_vs_term_in_bins_of_other_term = [] # one histogram for each pTtruth and eta bin for each term
  dpTresidual_dterm_vs_other_term          = [] # one histogram for each pTtruth and eta bin for each term
  dpTresidual_dterm_averaged_vs_abseta     = [] # one histogram for each pTtruth bin for each term

  ##########################################################
  # Open output config file with calibrations
  outConfigFile = open("Outputs/Config_"+cfg.Version+".config","w")
  Base = cfg.Version+"_"+cfg.JetAlg
  outConfigFile.write(Base+".Description: "+cfg.Version+" jet pile-up residual corrections\n")
  
  ##########################################################
  # Get binning from config
  InitialAbsEtaBinning = array.array('d',cfg.InitialAbsEtaBins)
  nInitialAbsEtaBins   = len(InitialAbsEtaBinning)-1
  FinalAbsEtaBins      = cfg.FinalAbsEtaBins
  nFinalAbsEtaBins     = len(FinalAbsEtaBins)-1
  PtBins               = cfg.PtBins
  nPtBins              = len(PtBins)-1
  nVar1Bins            = len(cfg.Variable1Bins)-1   # NPV or nJet10
  nVar2Bins            = len(cfg.Variable2Bins)-1   # Mu
  Var1Binning          = array.array('d',cfg.Variable1Bins) # NPV or nJet10
  Var2Binning          = array.array('d',cfg.Variable2Bins) # Mu
  Var1Min              = Var1Binning[0]
  Var1Max              = Var1Binning[nVar1Bins-1]
  Var2Min              = Var2Binning[0]
  Var2Max              = Var2Binning[nVar2Bins-1]

  ##########################################################
  # Setup TF1 for linear fits
  LinearFit_vs_Var1 = ROOT.TF1("LinearFit_vs_Var1","[0]+[1]*x",Var1Min,Var1Max)
  LinearFit_vs_Var1.SetParameter(0,0.)
  LinearFit_vs_Var1.SetParameter(1,0.)
  LinearFit_vs_Var2 = ROOT.TF1("LinearFit_vs_Var2","[0]+[1]*x",Var2Min,Var2Max)
  LinearFit_vs_Var2.SetParameter(0,0.)
  LinearFit_vs_Var2.SetParameter(1,0.)
  # Setup TF1 for constant fits
  Fit_average_over_Var1 = ROOT.TF1("Average_over_Var1","[0]",Var1Min,Var1Max)
  Fit_average_over_Var1.SetParameter(0,0.)
  Fit_average_over_Var2 = ROOT.TF1("Average_over_Var2","[0]",Var2Min,Var2Max)
  Fit_average_over_Var2.SetParameter(0,0.)

  ##########################################################
  # Write Final Abs Eta Bins into output config file
  AbsEtaBinsStr = ""
  for eta in FinalAbsEtaBins:
    AbsEtaBinsStr += str(eta)+" "
  outConfigFile.write(Base+".AbsEtaBins: "+AbsEtaBinsStr+"\n")

  ##########################################################
  # Get the pTresidual histogram vs Mu (NPV/nJet10) for each pTtruth, eta, and NPV/nJet10 (Mu) bin
  for ptbin in range(nPtBins): # loop over pTtruth bins

    ##########################################################
    # Loop over terms
    for term in Terms:

      # Book histogram of averaged dpTresidual/dterm averaged over other term vs abseta
      dpTresidual_dterm_averaged = ROOT.TH1D("dpTresidual_d"+term+"_averaged_over_otherterm_vs_abseta_ptbin_"+str(ptbin),"",nInitialAbsEtaBins,InitialAbsEtaBinning)

      ##########################################################
      # Loop over eta bins
      for etabin in range(nInitialAbsEtaBins):
	# Get TH3D histogram
        Hist3DName  = "pTresidual_vs_"
	if NPV:
          Hist3DName += "NPV"
        else:
          Hist3DName += "nJet"
        Hist3DName += "_vs_Mu_ptbin_"+str(ptbin)+"_etabin_"+str(etabin)
        Hist3D      = iFile.Get(Hist3DName)
        if not Hist3D:
          print "ERROR: "+Hist3DName+" not found, exiting"
  	  sys.exit(0)

	if term == "Mu":
	  nTermBins        = nVar2Bins
	  TermBinning      = Var2Binning
	  nOtherTermBins   = nVar1Bins
	  OtherTermBinning = Var1Binning
	  OtherTerm        = "NPV"
	  if nJet:
	    OtherTerm      = "nJet"
	  Project3D        = "zy"
	else:
	  nTermBins        = nVar1Bins
	  TermBinning      = Var1Binning
	  nOtherTermBins   = nVar2Bins
	  OtherTermBinning = Var2Binning
	  OtherTerm        = "Mu" 
	  Project3D        = "zx"

        # Book histogram of d<pTresidual>/dterm vs other term
	dpTresidual_dterm = ROOT.TH1D("dpTresidual_d"+term+"_vs_otherterm_ptbin_"+str(ptbin)+"_etabin_"+str(etabin),"",nOtherTermBins,OtherTermBinning)
	BaseName = "pTresidual_vs_"+term+"_ptbin_"+str(ptbin)+"_etabin_"+str(etabin)

        ##########################################################
        # Loop over bins of the other term
        for othertermbin in range(nOtherTermBins):
          # TH2 pTresidual vs term for the given bin of the other term
          Hist3D.GetXaxis().SetRange(0,0) # reset range
          Hist3D.GetYaxis().SetRange(0,0) # reset range
	  if term == "Mu":
	    Hist3D.GetXaxis().SetRangeUser(OtherTermBinning[othertermbin],OtherTermBinning[othertermbin+1]) # get the given bin of the other term
	  else:
	    Hist3D.GetYaxis().SetRangeUser(OtherTermBinning[othertermbin],OtherTermBinning[othertermbin+1]) # get the given bin of the other term
          Hist2D = Hist3D.Project3D(Project3D+"_"+OtherTerm+"bin_"+str(othertermbin))  # pTresidual vs term
	  Hist2D.Sumw2()
	  # Loop over term bins and get <pTresidual>
	  HistName = "MeanPtResidual_vs_"+term+"_ptbin_"+str(ptbin)+"_etabin_"+str(etabin)+"_"+OtherTerm+"bin_"+str(othertermbin)
	  MeanPtResidual_vs_term = ROOT.TH1D(HistName,"",nTermBins,TermBinning)
	  nBins2Fit = 0
	  for Bin in range(nTermBins):
            # Get <pTresidual> for this term bin
            Hist2D.GetXaxis().SetRangeUser(TermBinning[Bin],TermBinning[Bin+1])
	    if Hist2D.GetEffectiveEntries()<10: continue # skip this bin w/ low stats
	    nBins2Fit += 1
	    MeanPtResidual      = Hist2D.GetMean(2)      # Get mean of y-axis (pTresidual)
	    MeanPtResidualError = Hist2D.GetMeanError(2) # Get mean error of y-axis (pTresidual)
	    # Fill <pTresidual> vs term histogram (then we plot <pTresidual>)
	    MeanPtResidual_vs_term.SetBinContent(Bin+1,MeanPtResidual)
	    MeanPtResidual_vs_term.SetBinError(Bin+1,MeanPtResidualError)
          # Fit <pTresidual> vs term and push values to d<pTresidual>/dterm vs other term
	  if nBins2Fit < 3: continue # only fit bins with at least three points
	  if term == "Mu":
            fit = LinearFit_vs_Var2.Clone(BaseName+"_"+OtherTerm+"bin_"+str(othertermbin)+"_myfit") # fit to pTresidual vs mu
	  else:
            fit = LinearFit_vs_Var1.Clone(BaseName+"_"+OtherTerm+"bin_"+str(othertermbin)+"_myfit") # fit to pTresidual vs NPV/nJet
	  fitResult = MeanPtResidual_vs_term.Fit(fit)
          # Save MeanPtResidual vs term histograms
	  pTresidual_vs_term_in_bins_of_other_term.append(MeanPtResidual_vs_term)
	  # Extract fit result (d<pTresidual>/dterm)
	  if fitResult:
	    slope      = fit.GetParameter(1)
	    slopeError = fit.GetParError(1)
	    dpTresidual_dterm.SetBinContent(othertermbin+1,slope)
	    dpTresidual_dterm.SetBinError(othertermbin+1,slopeError)

	# Save d<pTresidual>/dterm vs other term histograms
	dpTresidual_dterm_vs_other_term.append(dpTresidual_dterm)
	# Take average of d<pTresidual>/dterm over the other term
	if term == "Mu":
          if NPV:
            scalarFit = Fit_average_over_Var1.Clone("dpTresidual_dMu_vs_NPV_ptbin_"+str(ptbin)+"_etabin_"+str(etabin)+"_myfit") # fit to pTresidual vs NPV
          else:
            scalarFit = Fit_average_over_Var1.Clone("dpTresidual_dMu_vs_nJet_ptbin_"+str(ptbin)+"_etabin_"+str(etabin)+"_myfit") # fit to pTresidual vs nJet
	else:
          if NPV:
            scalarFit = Fit_average_over_Var2.Clone("dpTresidual_dNPV_vs_Mu_ptbin_"+str(ptbin)+"_etabin_"+str(etabin)+"_myfit") # fit to pTresidual vs NPV
          else:
            scalarFit = Fit_average_over_Var2.Clone("dpTresidual_dnJet_vs_Mu_ptbin_"+str(ptbin)+"_etabin_"+str(etabin)+"_myfit") # fit to pTresidual vs nJet
	fitResult = dpTresidual_dterm.Fit(scalarFit)
	# Extract fit result (<d<pTresidual>/dMu>)
	if fitResult:
	  average      = scalarFit.GetParameter(0)
	  averageError = scalarFit.GetParError(0)
          dpTresidual_dterm_averaged.SetBinContent(etabin+1,average)
	  dpTresidual_dterm_averaged.SetBinError(etabin+1,averageError)
     
      # Save <d<pTresidual>/dterm> vs abseta histograms
      dpTresidual_dterm_averaged_vs_abseta.append(dpTresidual_dterm_averaged)
      # Fit dpTresidual/dterm vs eta with a linear function for each final eta bin
      FitFunctions = []
      Parameters   = []     # values that goes to the config file (only for the first pTtruth bin)
      ValueAtAbsEtaMin = 0  # this will be used to ensure continuity between the linear fits
      for etabin in range(nFinalAbsEtaBins):
        # Fit dpT/residual/dterm vs eta with a linear function for the range corresponding to etabin
	AbsEtaMin     = FinalAbsEtaBins[etabin]
	AbsEtaMax     = FinalAbsEtaBins[etabin+1]
        FitName       = "LinearFit_vs_eta_"+str(etabin)
	if etabin == 0:
          LinearFit_vs_eta = ROOT.TF1("LinearFit_vs_eta_"+str(etabin),"[0]+[1]*x",AbsEtaMin,AbsEtaMax)
	else:
          LinearFit_vs_eta = ROOT.TF1("LinearFit_vs_eta_"+str(etabin),"[0]+[1]*(x-[2])",AbsEtaMin,AbsEtaMax)
	if etabin != 0:
	  LinearFit_vs_eta.FixParameter(0,ValueAtAbsEtaMin)
	  LinearFit_vs_eta.FixParameter(2,AbsEtaMin)
	if doFinalVSetaFit:
	  if etabin == 0:
	    fitResult = dpTresidual_dterm_averaged.Fit(LinearFit_vs_eta,"R")
	  else:
	    fitResult = dpTresidual_dterm_averaged.Fit(LinearFit_vs_eta,"R+")
	else:
          fitResult = False
	# Extract fit result and prepare config file
	if fitResult:
          ValueAtAbsEtaMin = LinearFit_vs_eta.GetParameter(0)+LinearFit_vs_eta.GetParameter(1)*(AbsEtaMax-AbsEtaMin)
	  if ptbin == 0:
            if etabin == 0: Parameters.append(LinearFit_vs_eta.GetParameter(0))
	    Parameters.append(LinearFit_vs_eta.GetParameter(1))
        FitFunctions.append(LinearFit_vs_eta)
      # Write parameters to the config file
      ParametersStr = ""
      for parameter in Parameters: ParametersStr += str(format(parameter,'.3f'))+" "
      if term == "Mu":
        outConfigFile.write(Base+".MuTerm."+cfg.JetAlg.rstrip("Jets")+": "+ParametersStr+"\n")
      elif term == "nJet10":
        outConfigFile.write(Base+".nJetTerm."+cfg.JetAlg.rstrip("Jets")+": "+ParametersStr+"\n")
      elif term == "NPV":
        outConfigFile.write(Base+".NPVTerm."+cfg.JetAlg.rstrip("Jets")+": "+ParametersStr+"\n")

      # FIXME Store somehow the parameters for other pt bins
      # FIXME Protection when no fitResult

  outConfigFile.close()
  # Write histograms into a ROOT file
  Extra = ""
  if not doFinalVSetaFit:
    Extra = "_noFinalFit"
  oFile = ROOT.TFile("Outputs/TH1D_pTresidual_vs_term_"+cfg.Version+Extra+".root","RECREATE")
  for hist in pTresidual_vs_term_in_bins_of_other_term: hist.Write()
  for hist in dpTresidual_dterm_vs_other_term: hist.Write()
  for hist in dpTresidual_dterm_averaged_vs_abseta: hist.Write()
  oFile.Close()

  print "INFO: All Done"
  


