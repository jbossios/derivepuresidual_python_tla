#!/usr/bin/python
import os, sys
from ROOT import *

# PATH to ROOT files
PATH = "REPLACE/derivepuresidual_python_tla/Outputs/"
PATHtoAtlasStyle = "/afs/cern.ch/user/j/jbossios/work/public/xAOD/Results/AtlasStyle"

JetCollections = [
  "AntiKt4EMTopoJets",
]

NPVclosure = True

Versions = [
  "TLA_02122019_PythiaWithSW_BeforeAreaCorr_MC16e_NewPtTruthBin_NPV_noFinalFit",
  "TLA_17102019_PythiaWithSW_AfterAreaCorr_MC16e_NewPtTruthBin_useConstit_NPV_noFinalFit",
  "TLA_02122019_PythiaWithSW_AfterMuPUresidual_MC16e_NewPtTruthBin_NPV_noFinalFit",
]


Terms = [
  "Mu",
#  "nJet20",
#  "NPV",
#  "nJet10",
]

Formats = ["PDF","EPS","PNG","C"]

##################################################################################
# DO NOT MODIFY
##################################################################################

PATH += "/"
PATHtoAtlasStyle += "/"
gROOT.SetBatch(True)  # so does not pop up plots!
gROOT.LoadMacro(PATHtoAtlasStyle+"AtlasStyle.C")
SetAtlasStyle()
gROOT.ProcessLine(".L loader.C+") # So I can read vector<vector<float> > branches

#Colors  = [kBlue,kMagenta+1,kRed,kGreen+2]
Colors  = [kBlue,kMagenta+1,kRed,kBlack,kGreen+2]
#Markers = [20,21,22,23]
Markers = [20,21,22,34,23]

def parseline(ln):
    ls = list()
    desc = True 
    for w in ln.split():
        if desc and ':' in w:
            desc = False
            continue
        if not desc:
            ls.append(float(w))
    return ls

class linpiece_func:
  
  """
      class of linear piecewise functions (callable) constructed from the residual pielup calibration files.
      One instance oorresponds to a single calibration curve.
  """

  def __init__(self,calib,algo = 'AntiKt2',term = 'NPV'):
    self.algo = algo #for debuggingand testing
    self.term = term #for debuggingand testing
    self.slope = list() # calibration slope parameter values in som eAbsEta range
    self.etalo = list() # abseta bin lower edges of the calibration
    self.offs = list() # the value of the function at eta = etalo[i]
    # parse the file  line by line and find slope and etalo in there
    with open(calib,'r') as f:
      for l in f:
        if algo in l and term in l:
          if self.slope:
            print "Warning: Found more than one "  + term + " - term  calibration for " + algo + " jets in file  '" + calib + "'."
            print "Overwriting previously found calibration."
            self.slope = list()
          self.slope = parseline(l)
        elif 'AbsEtaBins' in l:
          if self.etalo:
            print "Warning: Found more than one set of AbsEtaBins in file '" + calib + "'."
            print "Overwriting previously found AbsEtaBins."
            self.etalo = list()
          self.etalo = parseline(l)
      if (not self.etalo) or (not self.slope):
        raise IOError('failed to parse ' + calib)
      # calculate offsets once when initailising, then hold in memory
      # do not recalcualte with every __call__
      self.offs.append(self.slope[0])
      for i in range(1,len(self.etalo)):
        es = self.etalo[i] - self.etalo[i-1]
        no = self.offs[i-1] + self.slope[i]*es
        self.offs.append(no)
      f.close()

  def __call__(self,x):
    y = float('nan')
    x = x[0]
    for i in range(len(self.etalo) -1):
      if x >= self.etalo[i] and x <self.etalo[i+1]:
        y = self.offs[i] + (x - self.etalo[i])*self.slope[i+1] 
        break
    return y

# Loop over jet collections
for jetColl in JetCollections:
  # Loop over terms
  for term in Terms:
    for format in Formats:	  
      Campaign = "MC16a"
      if "MC16d" in Versions[0]:
        Campaign = "MC16d"
      elif "MC16e" in Versions[0]:
        Campaign = "MC16e"
      elif "MC16All" in Versions[0]:
        Campaign = "MC16All"
      # Loop over curves	  
      counter = 0
      # TCanvas
      Can = TCanvas()
      outPDF  = PATH
      outPDF += term
      outPDF += "Term_"
      outPDF += jetColl
      outPDF += "_"
      outPDF += Campaign
      if NPVclosure:
        outPDF += "_NPVclosure"
      if format=="PDF":
        outPDF += ".pdf"
      elif format=="EPS":
        outPDF += ".eps"
      elif format=="PNG":
        outPDF += ".png"
      elif format=="C":
        outPDF += ".C"
      if "C" not in format:
        Can.Print(outPDF+"[")
      Hists   = []
      Bands   = []
      Legends = []
      for Version in Versions:
        # Open file
	FileName = PATH+"TH1D_pTresidual_vs_term_"+Version+".root"
        File     = TFile.Open(FileName,"READ")
        if not File:
          print FileName+" not found, exiting"
          sys.exit(0)
        # Get Histogram
	HistName = "dpTresidual_d"+term+"_averaged_over_otherterm_vs_abseta_ptbin_0"
        Hist = File.Get(HistName)
        if not Hist:
          print HistName+" not found, exiting"
          sys.exit(0)
        Hist.GetXaxis().SetTitle("|#eta|")
        if term == "Mu":
          Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial#mu [GeV]")
        elif term=="NPV":
          Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial N_{PV} [GeV]")
	elif term=="nJet20":
          Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial n_{20}^{jet} [GeV]")
	elif term=="nJet10":
          Hist.GetYaxis().SetTitle("#partial#it{p}_{T}/#partial n_{10}^{jet} [GeV]")
	if term == "Mu":
          Hist.SetMinimum(-0.8) # to compare with offline
          Hist.SetMaximum(0.9)  # to compare with offline
        elif term == "nJet20" or term == "nJet10":
          Hist.SetMinimum(-0.15)
          Hist.SetMaximum(0.3)
        else:
          Hist.SetMinimum(-0.12)
          Hist.SetMaximum(0.4)
        gStyle.SetErrorX(.5)
        gStyle.SetOptStat(0)
        Hist.SetLineColor(Colors[counter])
        Hist.SetMarkerStyle(Markers[counter])
        Hist.SetMarkerColor(Colors[counter])
        Hist.SetMarkerSize(2)
        Hist.SetDirectory(0)
        Hists.append(Hist)
	# Choose legend
        Text = ""
        if "BeforeAreaCorr" in Version:
          Text = "Before any correction"
        elif "AfterAreaCorr" in Version:
          Text = "After area-based correction"
        elif "AfterMu" in Version:
          Text = "After #mu-based residual correction"
	elif "AfterFull" in Version:
	  Text = "After full residual calibration"
        Legends.append(Text)
        counter += 1
      # TLegend
      Legend = TLegend(0.2,0.2,0.6,0.4)
      Legend.SetTextFont(42)
      counter = 0
      for hist in Hists:
        Legend.AddEntry(hist,Legends[counter])
        hist.Draw("Psame")
        counter += 1
      Legend.Draw("SAME")
      # ATLAS label
      AtlasLabel = "#font[72]{ATLAS} #font[42]{Simulation Internal}"
      x_1 = 0.2
      y_1 = 0.88
      step = 0.04
      TextBlock1 = TLatex(x_1,y_1,AtlasLabel)
      TextBlock1.SetNDC()
      TextBlock1.Draw("same")
      # Show CME and Sample
      if "Sherpa" not in Version:
        CME_and_Sample = "#sqrt{s} = 13 TeV, "+Campaign+" Pythia8 dijet"
      else:
        CME_and_Sample = "#sqrt{s} = 13 TeV, "+Campaign+" Sherpa dijet"
      TextBlock2 = TLatex(x_1,y_1-1.5*step,CME_and_Sample)
      TextBlock2.SetNDC()
      TextBlock2.Draw("same")
      # Show CME and Sample
      if "EMTopo" in jetColl:
        JetType = "Anti-k_{t} #it{R} = 0.4 (EMTopo)"
      if "EMPFlow" in jetColl:
        JetType = "Anti-k_{t} #it{R} = 0.4 (PFlow)"
      TextBlock3 = TLatex(x_1,y_1-3*step,JetType)
      TextBlock3.SetNDC()
      TextBlock3.Draw("same")
      # Draw line at y=0
      Line = TLine(0, 0, 4.9, 0)
      Line.SetLineStyle(2)
      Line.SetLineColor(kBlack)
      Line.Draw("same")
      if "C" not in format:
        Can.Print(outPDF)
        Can.Print(outPDF+"]")
      else:
	Can.SaveAs(outPDF)

print ">>> ALL DONE <<<"

