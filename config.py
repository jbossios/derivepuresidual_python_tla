#!/usr/bin/env python
class Config:
  def __init__(self):
    # NPV or nJet20
    self.Variable1     = 'NPV'
    self.Variable1Bins = [0,5,10,20]
    
    # mu
    self.Variable2     = 'actualInteractionsPerCrossing'
    self.Variable2Bins = [0,20,25,30,35,40,80]
    
    # jet truth pT binning
    #IPar0: jet_true_pt
    self.PtBins = [20,30]
    
    # Date of calibration
    self.Version = 'ExampleConfig_XXYYZZZZ'
    
    # Jet Algorithm
    self.JetAlg = 'AntiKt4EMTopoJets'
    
    # TreeName
    self.TreeName = 'TREENAME'
    
    # Apply area-based correction?
    self.ApplyAreaCorr = True

    # Rho
    self.Rho = 'rhoEM'

    # Eta binning for initial linear fits as function of mu/npv/njet20
    self.InitialAbsEtaBins = [0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.8,4.0,4.2,4.5,4.9]

    # Eta Binning for final linear fits as a function of eta
    self.FinalAbsEtaBins = [0,0.5,0.8,1.2,1.6,1.8,2.1,2.4,2.8,3.4,3.6,4.0,4.3,6.0]
    
    # Name for branches
    
    # Name of jet reco branches
    self.RecoPt  = 'jet_PileupPt'
    self.RecoEta = 'jet_PileupEta'
    self.RecoPhi = 'jet_PileupPhi'
    self.RecoE   = 'jet_PileupE'
    self.RecoA   = 'jet_ActiveArea4vec_pt'
    
    # Name of jet truth branches
    self.TruePt  = 'jet_true_pt'
    self.TrueEta = 'jet_true_eta'
    self.TruePhi = 'jet_true_phi'
    self.TrueE   = 'jet_true_e'

  def Print(self):
    message  = "######################################################\n"
    message += "INFO: Configuration:\n"
    message += "\t Variable1: "+self.Variable1+"\n"
    message += "\t Variable1Bins: "+str(self.Variable1Bins)+"\n"
    message += "\t Variable2: "+self.Variable2+"\n"
    message += "\t Variable2Bins: "+str(self.Variable2Bins)+"\n"
    message += "\t PtBins: "+str(self.PtBins)+"\n"
    message += "\t Version: "+self.Version+"\n"
    message += "\t JetAlg: "+self.JetAlg+"\n"
    message += "\t TreeName: "+self.TreeName+"\n"
    if self.ApplyAreaCorr:
      message += "\t ApplyAreaCorr: True\n"
      message += "\t Rho: "+self.Rho+"\n"
    else: message += "\t ApplyAreaCorr: False\n"
    message += "\t InitialAbsEtaBins: "+str(self.InitialAbsEtaBins)+"\n"
    message += "\t FinalAbsEtaBins: "+str(self.FinalAbsEtaBins)+"\n"
    message += "\t RecoPt branch name: "+self.RecoPt+"\n"
    message += "\t RecoEta branch name: "+self.RecoEta+"\n"
    message += "\t RecoPhi branch name: "+self.RecoPhi+"\n"
    message += "\t RecoE branch name: "+self.RecoE+"\n"
    message += "\t RecoA branch name: "+self.RecoA+"\n"
    message += "\t TruePt branch name: "+self.TruePt+"\n"
    message += "\t TrueEta branch name: "+self.TrueEta+"\n"
    message += "\t TruePhi branch name: "+self.TruePhi+"\n"
    message += "\t TrueE branch name: "+self.TrueE+"\n"
    message += "######################################################\n"
    return message
