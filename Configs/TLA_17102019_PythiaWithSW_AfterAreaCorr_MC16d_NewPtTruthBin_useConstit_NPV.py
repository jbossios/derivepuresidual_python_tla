# Example config file to derive calibrations for the TLA analysis

#!/usr/bin/env python
import config as cfg
Config = cfg.Config()

# NPV
Config.Variable1     = 'NPV'
Config.Variable1Bins = [0,10,14,18,22,44]

# mu
Config.Variable2     = 'actualInteractionsPerCrossing'
Config.Variable2Bins = [0,15,20,25,30,35,40,45,50,55,60,100]

# jet truth pT binning
Config.PtBins = [60,80]

# Date of calibration
Config.Version = 'TLA_17102019_PythiaWithSW_AfterAreaCorr_MC16d_NewPtTruthBin_useConstit_NPV'

# Jet Algorithm
Config.JetAlg = 'AntiKt4EMTopoJets'

# TreeName
Config.TreeName = 'IsolatedJet_tree'

# Apply area-based correction?
Config.ApplyAreaCorr = False

# Eta binning for initial linear fits as function of mu/npv/njet20
Config.InitialAbsEtaBins = [0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0,3.1,3.2,3.3,3.4,3.5,3.6,3.8,4.0,4.2,4.5,4.9]

# Eta Binning for final linear fits as a function of eta
#Config.FinalAbsEtaBins = [0,0.5,0.8,1.2,1.6,1.8,2.1,2.4,2.8,3.4,3.6,4.0,4.3,6.0]
#Config.FinalAbsEtaBins = [0,0.5,0.8,1.2,1.6,1.8,2.1,2.4,2.8,3.4,3.6,4.0,4.5,6.0]
#Config.FinalAbsEtaBins = [0,0.5,0.8,1.2,1.6,1.8,2.1,2.4,2.8,3.4,3.6,3.8,4.0,4.5,6.0]
Config.FinalAbsEtaBins = [0,0.5,0.8,1.2,1.6,1.8,2.1,2.4,2.8,3.4,3.6,3.7,4.5,6.0]

# Name for branches

# Name of jet reco branches
Config.RecoPt  = 'jet_PileupPt'
Config.RecoEta = 'jet_PileupEta'
Config.RecoPhi = 'jet_PileupPhi'
Config.RecoE   = 'jet_PileupE'
Config.RecoA   = 'jet_ActiveArea4vec_pt'

# Name of jet truth branches
Config.TruePt  = 'jet_true_pt'
Config.TrueEta = 'jet_true_eta'
Config.TruePhi = 'jet_true_phi'
Config.TrueE   = 'jet_true_e'

