################################################
# Setup
################################################
$ source Setup.sh

################################################
# Derive calibration
################################################

# Procedure used to derive MC16a+d+e calibration (read carefully!)
# Step 1 was run separately for each MC16 campaign (using a dedicated config for each campaign)
# Step 4 was run merging all the TH3D histograms from all MC16 campaigns with Step 3 (using a dedicated MC16All config which is used only in this step)
# In Step 3, any MC16 config can be used, except for the MC16All config!

# NOTE: If a sequential mu + NPV/nJet calibration is derived, then the final config file of JetCalibTools which is prepared by this script should be modified to incorporate the mu calibration
# otherwise it will contain the parametrization of the mu residual after applying the mu calibration

# 0. You need to prepare a configuration file
# An example can be found in Configs/TLA_Example.py
'Variable1' should be the name of the branch in the TTrees for NPV or nJet
'Variable2' should be the name of the branch in the TTrees for mu
'Variable1Bins' and 'Variable2Bins' are the arrays of bins for Variable1 and Variable2, respectively
'PtBins' is the array of bins for pT
'Version' is name helpful name to identify a given calibration (for example: 'XXYYZZZZ_BeforeAreaCorr_MC16a')
'JetAlg' this should match the name it will be used to setup JetCalibTools (it really does not matter the name, I used 'AntiKt4EMTopoJets')
'TreeName' is the name of the TTree in the input ROOT files
'ApplyAreaCorr' should be False since for HLT/TLA jets it is not possible to re-apply the area-based pileup correction (jets at the pileup scale are used to derive the calibration)
'InitialAbsEtaBins' is the eta binning for initial linear fits as function of mu/npv/njet20
'FinalAbsEtaBins' is the eta binning for final linear fits as a function of eta
'RecoPt', 'RecoEta', 'RecoPhi', 'RecoE' and 'RecoA' are the name of the branches for reco jet pT, jet eta, jet phi, jet energy and jet area
'TruePt', 'TrueEta', 'TruePhi' and 'TrueE' are the name of the branches for truth jet pT, jet eta, jet phi and jet energy

# 1. Produce histograms
# This option will produce the needed TH3D histograms of pTresidual vs mu and NPV/nJet
'FillpTresidualHists' should be True
# Choose the configuration file with the following
'Config'
# Choose input files with the following list
'InputFiles'
# Choose the two variables which will be used to derive the calibration with the following list (options: Mu and NPV or Mu and nJet10)
'Terms'

# 2. Merge ROOT files with histograms
# This will merge the ROOT files produced in the previous step
'MergepTresidualHists' should be True
# Choose the configuration file with the following
'Config'
# This step will merge the output file for each file defined in the following list
'InputFiles'

# 3. A calibration can be derived for each MC campaign separately (optional)
# but if you want to derive a calibration for more than one MC campaign together
# then you need to run the following step
'MergeMC16campaigns' should be True

# 4. Derive calibration
# This will perform all the fits and prepare the config file for JetCalibTools
'DeriveCalibration' should be True

# If you want to use the 'FinalPlots.py' script, then you need to disable the following
# Note that the config for JetCalibTools should be produced with this ON
'doFinalVSetaFit'

#########
# Plots
#########
# There are two scripts
'FinalPlots.py' produces the plots where the calibrations for each step are compared
'MakeIntermediatePlots.py' produces all plots for all the intermediate steps of the derivation chain
